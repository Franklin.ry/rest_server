//Modulos  & Dependencias
const Role = require('../models/role');
const Usuario = require('../models/usuario');



const esRoleValido = async(rol = '') => {
    const existeRol = await Role.findOne({ rol });

    // console.log(existeRol);

    if (!existeRol) {
        // error validaciones personalizadas
        throw new Error(`El rol: ${rol} no esta permitido`);
    }



};

// Verificar si existe el correo
const emailExiste = async(correo = '') => {

    const existeEmail = await Usuario.findOne({ correo });

    if (existeEmail) {

        throw new Error(`El correo: ${correo} ya esta registrado`);

    }
};

// Verificar si existe el correo
const existeUsuariosPorId = async(id) => {

    const existeUsuario = await Usuario.findById(id);

    if (!existeUsuario) {

        throw new Error(`El id no existe : ${id} en DB`);

    }
};



module.exports = {

    esRoleValido,
    emailExiste,
    existeUsuariosPorId
};