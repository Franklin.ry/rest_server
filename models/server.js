const express = require('express');
const cors = require('cors');
const { dbConnection } = require('../database/config');

//Clase mantener app.js

class Server {

    constructor() {
        this.app = express();
        this.port = process.env.PORT;
        this.usuariosPath = '/api/usuarios';
        this.authPath = '/api/auth';

        //Conectar a DB

        this.conectarDB();

        //Middlewares
        this.middlewares();

        //Lectura y  parse del body
        this.app.use(express.json());

        //Rutas de la aplicación
        this.routes();

    }

    middlewares() {

        //CORS
        this.app.use(cors());

        //Directorio Público
        this.app.use(express.static('public'));
    }

    async conectarDB() {

        await dbConnection();

    }

    routes() {

        this.app.use(this.authPath, require('../routes/auth'));
        this.app.use(this.usuariosPath, require('../routes/usuarios'));

    }


    listen() {
        this.app.listen(this.port, () => {
            console.log(`Servidor corriendo en puerto ${this.port}`);
        });
    }

}

module.exports = Server;