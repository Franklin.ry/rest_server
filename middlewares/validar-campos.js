const { validationResult } = require('express-validator');

//El middleware personalizado nesecita tres argumentos req,res,next
// next -> Sirve para avanzar a los middlewares 
const validarCampos = (req, res, next) => {

    const errors = validationResult(req);
    if (!errors.isEmpty()) {

        return res.status(400).json(errors);
    }

    next();
};




module.exports = {
    validarCampos
};