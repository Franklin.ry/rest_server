//Conexion  a db 
const moongose = require('mongoose');

const dbConnection = async() => {

    try {

        await moongose.connect(process.env.MONGODB_CNN, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
            useFindAndModify: false

        });

        console.log('Base datos  conectada con exito');

    } catch (error) {

        console.log(error);

        throw new Error('Error en la conexion Base Datos');

    }

};


module.exports = {

    dbConnection
};