const { response, request } = require('express');
const bcryptjs = require('bcryptjs');

const Usuario = require('../models/usuario');


const usuariosGet = async(req = request, res = response) => {

    const { limite = 6, desde = 0 } = req.query;
    const query = {
        estado: true,
        _id: { $ne: req.usuario.id }
    };

    //$ne significa que es diferentes 
    const [total, usuarios] = await Promise.all([
        Usuario.countDocuments(query),
        Usuario.find(query)
        .skip(Number(desde))
        .limit(Number(limite))
    ]);

    res.json({
        total,
        usuarios
    });

};

const usuariosPost = async(req, res) => {

    const { nombre, correo, password, rol } = req.body;
    const usuario = new Usuario({ nombre, correo, password, rol }); //obtener data el body

    //Encriptar la contrasenia
    const salt = bcryptjs.genSaltSync();
    usuario.password = bcryptjs.hashSync(password, salt);

    //Guardar

    await usuario.save();

    res.json({

        msg: "Usuario Registrado con Exito",
        usuario
    });
};

const usuariosPut = async(req, res) => {

    const { id } = req.params;

    const { _id, password, google, correo, ...resto } = req.body;

    //Validar  en base datos
    if (password) {
        //Encriptar la contrasenia
        const salt = bcryptjs.genSaltSync();
        resto.password = bcryptjs.hashSync(password, salt);
    }

    const usuario = await Usuario.findByIdAndUpdate(id, resto);

    res.json({
        usuario,
        msg: "put API - controller"
    });
};

const usuariosPatch = (req, res) => {
    res.json({
        ok: true,
        msg: "patch API - controller"
    });
};

const usuariosDelete = async(req, res) => {

    const { id } = req.params;

    //Borrar fisicamente
    //const usuario = await Usuario.findByIdAndDelete(id);
    const usuario = await Usuario.findByIdAndUpdate(id, { estado: false });

    //Buscar Usuario Autenticado al uid
    //const usuarioAutentificado = req.usuario;


    res.json({
        usuario,
        msg: "delete API - controller"
    });
};




module.exports = {
    usuariosGet,
    usuariosPost,
    usuariosPut,
    usuariosPatch,
    usuariosDelete,
};