const { response, request } = require('express');

const bcryptjs = require('bcryptjs');
const Usuario = require('../models/usuario');
const { generarJWT } = require('../helpers/generar-jwt');

//Controlador de Auntentificación 

const login = async(req, res = response) => {


    const { correo, password } = req.body;

    try {

        //Verficar si correo existe
        const usuario = await Usuario.findOne({ correo });

        if (!usuario) {
            return res.status(400).json({
                msg: 'Usuario  / Password no son correctos --correo'
            });
        }

        //El usuario esta activo
        if (!usuario.estado) {
            return res.status(400).json({
                msg: 'Usuario  / Password no son correctos --estado false'
            });
        }


        //Verificar la contrasenia

        const validPassword = bcryptjs.compareSync(password, usuario.password);
        if (!validPassword) {
            return res.status(400).json({
                msg: 'Usuario  / Password no son correctos --password incorrecto'
            });
        }

        //Generar JWT
        const token = await generarJWT(usuario.id);

        res.json({
            usuario,
            token,
            msg: 'Login OK'
        });

    } catch (error) {
        return res.status(500).json({
            msg: "Hable con el administrador"
        });
    }


};

const renewToken = async(req = request, res = response) => {

    const uid = req.usuario.id;

    const token = await generarJWT(uid);

    const usuario = await Usuario.findById(uid);

    res.json({
        ok: true,
        usuario,
        token,
        msg: 'Renew-Token '
    });
};


module.exports = {
    login,
    renewToken
};